SELECT
	master.vehiclestatic.reg_number,
	fuel.id_vehiclestatic, fuel."timestamp",
	fuel.total_fuel_used AS materialzed_view_fuel, fuel.odometer AS materialzed_view_distance,
	acce.acceleration AS materialzed_view_acceleration, speed.speed AS materialzed_view_speed, accent.altitude_ascent AS materialzed_view_ascent, (180*(ASIN((accent.altitude_ascent)/(1000*fuel.odometer))))/pi() AS materialized_view_elevation_angle_asin,

	analytics.speed_drive_cycle_stats->'mean' AS analytics_speed_mean, analytics.speed_drive_cycle_stats->'meanRun' AS analytics_speed_mean_run,
	analytics.acceleration_drive_cycle_stats->'mean' AS analytics_acceleration_mean, analytics.acceleration_drive_cycle_stats->'meanRun' AS analytics_acceleration_mean_run,
	analytics.altitude_drive_cycle_stats->'elevationMeanPos' AS analytics_elevation_mean_positive
	
	FROM master.fuel_distance_sums_per_vehicle_per_day AS fuel
	
	JOIN master.average_acceleration_per_vehicle_per_day as acce ON acce.id_vehiclestatic = fuel.id_vehiclestatic AND acce.timestamp = fuel.timestamp
	JOIN master.average_speed_per_vehicle_per_day as speed ON speed.id_vehiclestatic = fuel.id_vehiclestatic AND speed.timestamp = fuel.timestamp
	JOIN master.ascent_descent_per_vehicle_per_day as accent ON accent.id_vehiclestatic = fuel.id_vehiclestatic AND accent.timestamp = fuel.timestamp
	JOIN master.vehiclestatic on fuel.id_vehiclestatic = master.vehiclestatic.id
	
	JOIN master.analyticsdata as analytics ON analytics.id_vehiclestatic = fuel.id_vehiclestatic AND analytics."timestamp" = fuel."timestamp"
	
	WHERE fuel.id_vehiclestatic in (956, 961, 963, 965, 968, 981, 958, 960,	964, 967, 969, 980)
	AND fuel.timestamp BETWEEN '2020-06-23' AND '2020-08-24 23:23:59'
	AND accent.altitude_ascent > 0
	
	ORDER by fuel.id_vehiclestatic, fuel."timestamp"
	;