# README #

This repository is to hold code and associated data required to evaluate the performance of the analytics system.

## Install ##

1. Python 3
1. Conda or pip


## Workflow ## 

Use conda lab.


## Project layout ## 

### dataMatview ### 

Hold data from matviews.

### dataPython ###

Hold data from Python processing.

### 

