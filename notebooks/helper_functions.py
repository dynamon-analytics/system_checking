#!/usr/bin/python3
"""
Helper functions for processing aggregated telematics data.
"""
import os
import platform
import pickle
import pandas as pd

# change the filesystem if it windows
if platform.system() == "Windows":
    path_parent = os.path.dirname(os.getcwd())
    os.chdir(path_parent)


# set hardcoded file directories :(
tomDir = "/Users/thomasdickson/Documents/system_checking/"
tomDataDir = tomDir + "dataPython/"
tomOutputData = tomDir + "outputData/"


def create_aggregated_dataframes(dictionary_dataframes):
    """Create one dataframe from dictionary of dataframes."""
    dict_dataframe = pickle.load( open(dictionary_dataframes, "rb" ) )
    return pd.concat([df.assign(idx=key) for key, df in dict_dataframe.items() if len(df.index.values) > 1],
                      ignore_index=True)


def load_python():
    """Load the aggregated daily data processed by the Python dataanalytics process."""
    filepath = tomDir + "dataPython/python_vehicle_data_aggregated.csv"
    if platform.system() == "Windows":
        filepath = "/dataPython/python_vehicle_data_aggregated.csv"
    dataPython = pd.read_csv(filepath, index_col=0)
    # dataPython.columns = [str(col) + '_python' for col in dataPython.columns]
    dataPython['timestamp'] = pd.to_datetime(dataPython['date'])
    dataPython['date'] = pd.to_datetime(dataPython['date'])
    dataPython.set_index('timestamp', inplace=True) 
    return dataPython


def loadMatview():
    """Load Matview"""
    filepath = tomDir + "dataMatview/mat_view_and_analytics_drive_cycle_stats_20200623_2020_08_24.csv"
    if platform.system() == 'Windows':
        filepath = "dataMatview/mat_view_and_analytics_drive_cycle_stats_20200623_2020_08_24.csv"
    dataMatview = pd.read_csv(filepath)
    dataMatview['timestamp'] = pd.to_datetime(dataMatview['timestamp'])
    dataMatview['date'] = pd.to_datetime(dataMatview['timestamp'].dt.date)
    dataMatview.set_index('timestamp', inplace=True)
    dataMatview = dataMatview.dropna()
    return dataMatview


